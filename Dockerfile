FROM alpine:latest

RUN apk add --update \
    borgbackup \
    openssh-client \
    tzdata \
    && rm -rf /var/cache/apk/*

RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    echo "Europe/Berlin" > /etc/timezone

CMD tail -f /dev/null

ENTRYPOINT ["borg"]